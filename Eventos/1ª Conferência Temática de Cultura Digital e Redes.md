Enviada a [proposta Dinheiro Público, Código Público](https://plantaformas.org/assemblies/culturadigitalbrasileira/f/50/proposals/74) 

"Propõe a discussão da implantação no Brasil da Campanha [Public Money Public Code](https://publiccode.eu/pt/), iniciada pela Free Software Foundation Europe, que pleiteia implementação de legislação requerendo que software financiado com dinheiro público, desenvolvido para o setor público, seja publicado com uma licença de Software Livre. O objetivo é que todo o código desenvolvido a partir de financiamento público receba uma licença de Software Livre e permaneça disponível publicamente para utilização, modificação e compartilhamento, como essas licenças exigem.

[Apresentada](https://www.youtube.com/live/yI6drHFmT9w?si=FafdbNzUjd_K4PFp&t=4316) na conferência em 26 de janeiro de 2024.
