Propostas:
    
1 . Investimento em produção tecnológica brasileira e proteção dos dados dos cidadãos.
O Governo Federal deve priorizar a adoção de soluções tecnológicas oferecidas por empresas e soluções de serviço Brasileiros ou, quando membros de um conglomarado econômico maior, cujos sócios majoritários e controladores sejam pessoas físicas e/ou jurídicas localizadas no Brasil. Além disso, deve também garantir que o software usado nestas soluções tecnológicas seja aberto, público e auditável, tendo sempre na máxima estimativa o bem-estar coletivo, o respeito à privacidade e à inclusão social .
(Sugestão de trilha: Planejamento e Orçamento)

2 . Fomento à adoção e produção de ecossistemas baseados em tecnologias abertas, visando estimular o desenvolvimento tecnológico brasileiro e a proteção dos dados dos cidadãos.
O Governo Federal deve priorizar o desenvolvimento e o recrutamento de tecnologias digitais em softwares de código aberto, recepção e/ou suportados por reservas de serviço brasileiros, aplicando recursos para o desenvolvimento do ecossistema nacional, calcado na segurança e privacidade dos dados e na possibilidade de auditoria plena dos códigos. Todo código desenvolvido deve ser disponibilizado, sob uma licença livre, para permitir seu reuso e desenvolvimento contínuo . 
(Sugestão de trilha: Indústria, Comércio e Serviço) 

3 . Inovação e Liberdade: Serviço Digital Brasileiro - Soluções Livres para Desafiar as BigTechs com Ancestralidade, Solidariedade e Felicidade para TODES 
Criação do Serviço Digital Brasileiro, para provimento de Plataformas Digitais de Interesse Público como alternativas nacionais às BigTechs. Baseado em softwares livres, com arquitetura distribuída e colaborativa, busca garantir privacidade, segurança e controle dos dados. Promover a economia solidária local, a inclusão digital, a autonomia tecnológica e a inovação Glocal. Fortalece a ancestralidade e a diversidade, emancipando todas as pessoas. Vem pra Rede: https://t.me/ComunidadeSoftwareLivre
(publicada em Ciência, Tecnologia e Inovação)

4 . Ampliar o papel da RNP (oq é "RNP"? https://www.rnp.br/?) para torná-la um órgão de apoio e estímulo ao desenvolvimento de plataformas digitais distribuídas e livres
A missão da RNP é "Promover o uso de redes avançadas inovadoras". Diante disso, ela deveria ampliar a sua atuação, estimulando a criação de redes de dados descentralizadas e federadas, experimentando em softwares livres, visando a interconexão de instituições públicas em uma rede federada nacional. Os nós dessa rede tínhamos diferentes graus de integração de dados e éramos gerenciados pelas instituições correspondentes. Caberia à RNP coordenar os esforços e fornecer orientações às motivadas .
(Sugestão de trilha: Comunicações)

5. Dinheiro Público, Código Público: Inovação e Soberania Tecnológica para o Brasil 
O futuro do Brasil está nas mãos da inovação e do conhecimento. "Dinheiro Público, Código Público" busca impulsionar nosso desenvolvimento tecnológico através do uso de código aberto e software livre. Essa abordagem promove colaboração, transparência e democratização do conhecimento. Ao investir em soluções de código público, garantimos a soberania tecnológica, permitindo que o Brasil crie, adapte e aprimore tecnologias de acordo com nossas necessidades e valores. Por um futuro digital inclusivo
(publicada em Ciência, Tecnologia e Inovação)

Proposta 6 a partir de elementos das propostas 3 e 4.
6.  Fomentar o Software Livre, cooperativismo e infraestrutura de rede pública e distribuída para soberania digital popular com valorização do trabalho. 
Criar nuvem pública em parceria com institutos/universidades federais, Dataprev, RNP, Correios, hospedando software livre/público, apoiar a inovação social através de cooperativas e fornecer serviços digitais que promovam a economia solidária, inclusão digital, autonomia tecnológica e inovação, fortalecendo a diversidade cultural, a formação crítica no uso das tecnologias e emancipando a todos com a valorização dos trabalhadores contra a precarização como alternativas nacionais às BigTechs.
(Sugestão de trilha: Infraestrutura)

Remix da proposta anterior
9. Software Livre, cooperativismo e infraestrutura de rede pública e distribuída para soberania digital com transparência e valorização do trabalho
Promover a soberania tecnológica e a literacia digital em ambientes seguros e robustos através do desenvolvimento de infraestruturas físicas e lógicas incentivadas por programas e parcerias com os mais diversos setores da sociedade e direcionadas ao fomento de inovação em hardware e software livre, redes e serviços de código aberto, fortalecendo a emancipação cidadã pela valorização de arranjos cooperativos, transparentes, solidários, diversos e distribuídos. Mais em: https://rb.gy/y3iuo

7. Tecnologia Viva: programa de políticas públicas voltadas para a soberania digital do país com transparência, autonomia e valorização do trabalho
Promover acesso e desenvolvimento tecnológico através de um programa de fomento a projetos e parcerias com estados, municípios e os mais diversos setores da sociedade que fortaleçam a emancipação cidadã através da valorização de arranjos cooperativos, transparentes, solidários, diversos e distribuídos, focados na inovação em infraestruturas de rede, hardware e software livre, nuvens e serviços de código aberto, em ambientes saudáveis, com dados seguros e soberanos. Mais em: https://rb.gy/y3iuo
(sugestão de trilha: Gestão e inovação)
