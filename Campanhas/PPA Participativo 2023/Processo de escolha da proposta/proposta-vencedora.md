Criação do Serviço Digital Brasileiro

Inovação e Liberdade: Soluções Livres para Desafiar as BigTechs com Ancestralidade, Solidariedade e Felicidade para TODES

Resumo:

A presente proposta visa estabelecer o Serviço Digital Brasileiro, uma iniciativa voltada para o desenvolvimento de Plataformas Digitais de Interesse Público no Brasil. Essas plataformas serão alternativas nacionais às grandes empresas de tecnologia (BigTechs), sendo baseadas em softwares livres, com uma arquitetura distribuída e colaborativa. O objetivo é garantir privacidade, segurança e controle dos dados dos usuários, promovendo também a economia solidária local, a inclusão digital, a autonomia tecnológica e a inovação Glocal. Além disso, o projeto busca fortalecer a ancestralidade e a diversidade, emancipando todas as pessoas. Convidamos você a fazer parte dessa iniciativa transformadora, junte-se à nossa comunidade no Telegram: https://t.me/ComunidadeSoftwareLivre.

Objetivos:

1. Desenvolvimento de Plataformas Digitais de Interesse Público: O Serviço Digital Brasileiro irá criar e disponibilizar uma variedade de plataformas digitais voltadas para atender às necessidades da sociedade brasileira. Essas plataformas abrangerão áreas como educação, saúde, cultura, governança participativa, comércio justo, entre outras, visando melhorar a vida das pessoas e fortalecer a democracia digital.

2. Baseado em Softwares Livres: Todas as plataformas desenvolvidas pelo Serviço Digital Brasileiro serão baseadas em softwares livres e de código aberto. Isso permitirá maior transparência, segurança e customização, além de incentivar a colaboração e o compartilhamento de conhecimento.

3. Arquitetura Distribuída e Colaborativa: O Serviço Digital Brasileiro adotará uma arquitetura distribuída, descentralizada e colaborativa para suas plataformas. Isso significa que os dados e as funcionalidades serão distribuídos em diferentes servidores, garantindo maior resiliência e evitando a concentração de poder nas mãos de poucas empresas.

4. Privacidade, Segurança e Controle dos Dados: A proteção da privacidade e dos dados dos usuários será uma prioridade no Serviço Digital Brasileiro. Serão implementadas medidas de segurança robustas e os usuários terão o controle total sobre suas informações, podendo decidir como e quando compartilhá-las.

5. Economia Solidária Local: O projeto busca promover a economia solidária local, fomentando o desenvolvimento de pequenos negócios e empreendimentos baseados nas plataformas digitais do Serviço Digital Brasileiro. Isso contribuirá para a criação de empregos, o fortalecimento da comunidade e a redução das desigualdades sociais e regionais.

6. Inclusão Digital e Autonomia Tecnológica: O Serviço Digital Brasileiro terá como objetivo promover a inclusão digital, garantindo o acesso equitativo e universal às suas plataformas. Além disso, o projeto visa desenvolver a autonomia tecnológica dos cidadãos, capacitando-os a compreender, utilizar e contribuir para a tecnologia de forma ativa.

7. Fortalecimento da Ancestralidade e Diversidade: Reconhecendo a importância da ancestralidade e da diversidade cultural do Brasil, o Serviço Digital Brasileiro valorizará e promoverá a inclusão de diferentes perspectivas e saberes em suas plataformas. Isso será feito por meio de parcerias com comunidades tradicionais, povos indígenas, quilombolas e outras expressões culturais presentes no país.

8. Emancipação das Pessoas: O Serviço Digital Brasileiro busca a emancipação das pessoas, permitindo que elas tenham controle sobre sua própria presença digital, possam exercer sua cidadania de forma plena e sejam protagonistas na construção de um futuro digital mais justo e igualitário.

Contamos com seu apoio e participação nessa jornada rumo a um Serviço Digital Brasileiro inovador, livre e inclusivo. Junte-se a nós e faça parte da transformação. Para mais informações e para se conectar à nossa comunidade, visite o seguinte link: https://t.me/ComunidadeSoftwareLivre.

Estamos ansiosos para construir um futuro digital melhor juntos!

Atenciosamente,

Comunidade Software Livre Brasil

Adicione seu nome ou a instituição que gostaria de fazer parte dessa iniciativa:

Uirá Porã - Hacker - Ceará;

Instituto Brasileiro de Políticas Digitais - Mutirão;

João Paulo Mehl

Diego Rojas - Designer - São Paulo

Instituto Pombas Urbanas

---------------------------------------------------------------------------------------

Dentro dessa proposta, o Instituto Brasileiro de Políticas Digitais - Mutirão, elaborou a seguinte Nota Técnica no intuito de iniciar as discussões sobre a implementação das estratégias propostas:

---------------------------------------------------------------------------------------

Nota Técnica:

Proposta de Criação do Serviço Digital Brasileiro

Plataformas Digitais de Interesse Público

Destinatário: Presidente Luís Inácio Lula da Silva

Data: 13 de Maio de 2023

Assunto: Proposta de Criação do Serviço Digital Brasileiro - Plataformas Digitais de Interesse Público

Prezados,

Por meio desta nota técnica, gostaríamos de apresentar uma proposta mais focada e específica para a criação do Serviço Digital Brasileiro, com ênfase em plataformas digitais de interesse público. Essa iniciativa tem como objetivo fornecer serviços digitais básicos do dia-a-dia das pessoas, como alternativas nacionais às grandes plataformas estrangeiras, e promover a utilização de softwares livres, arranjos distribuídos e colaborativos.

1. Oportunidade:

No cenário atual, observamos a dependência crescente de grandes plataformas estrangeiras para o acesso a serviços digitais essenciais. Essa dependência pode trazer desafios em termos de privacidade, segurança e controle dos dados dos usuários, bem como a falta de diversidade e autonomia tecnológica. Nesse contexto, surge a oportunidade de criar um Serviço Digital Brasileiro, com foco em plataformas digitais de interesse público, fornecendo alternativas nacionais e promovendo a utilização de softwares livres, arranjos distribuídos e colaborativos.

2. Proposta:

Propomos a criação do Serviço Digital Brasileiro - Plataformas Digitais de Interesse Público, que será responsável por desenvolver e fornecer serviços básicos do cotidiano das pessoas, tais como plataformas de entrega e transporte por aplicativo, marketplaces, redes sociais, sites, microblogs, entre outros. Essas plataformas seriam construídas com base em softwares livres e adotariam uma abordagem distribuída, colaborativa e orientada pela eficiência técnica e científica, em vez da busca por lucro.

As principais características desse serviço seriam:

a) Fornecimento de serviços básicos do dia-a-dia: Desenvolvimento e disponibilização de plataformas digitais para funções essenciais, como delivery, transporte, comunicação, compartilhamento de fotos, edição de documentos, videoconferência, entre outros.

b) Utilização de softwares livres: Adoção de softwares de código aberto e licenças livres, garantindo transparência, segurança e flexibilidade nas soluções desenvolvidas.

c) Arquitetura distribuída e colaborativa: Construção de arranjos tecnológicos que incentivem a descentralização e a colaboração entre usuários, desenvolvedores e instituições, promovendo a diversidade e a autonomia tecnológica.

d) Protocolos Abertos: A implementação e normatização de protocolos abertos/livres para os serviços visam garantir a interoperabilidade entre o Serviço Digital Brasileiro e outras plataformas afins, incluindo as plataformas comerciais, permitindo instâncias federadas e governança colaborativa na gestão do serviço.

e) Legislação e Políticas Digitais: Será necessária a implementação de uma frente normativa que regule o uso de protocolos abertos por todas as empresas, grandes serviços, governos e sociedade civil.

F) Economia local e distribuída: Estímulo ao desenvolvimento de negócios locais e distribuídos, gerando emprego e fomentando a economia nas diferentes regiões do país.

3. Benefícios:

A criação do Serviço Digital Brasileiro - Plataformas Digitais de Interesse Público traria benefícios significativos para a sociedade brasileira, tais como:

a) Soberania tecnológica: Ao fornecer alternativas nacionais para os serviços digitais básicos, o Serviço Digital Brasileiro promoverá a autonomia tecnológica do país, reduzindo a dependência de plataformas estrangeiras e garantindo maior controle sobre os dados e a infraestrutura tecnológica.

b) Privacidade e segurança: Com o uso de softwares livres e arranjos distribuídos, as plataformas digitais oferecidas pelo Serviço Digital Brasileiro serão projetadas para garantir a privacidade e a segurança dos dados dos usuários, protegendo suas informações pessoais.

c) Inclusão digital: O acesso a plataformas digitais de interesse público, por meio do Serviço Digital Brasileiro, promoverá a inclusão digital de todos os cidadãos, independentemente de sua localização ou recursos financeiros, contribuindo para a redução da exclusão digital.

d) Estímulo à economia local: Ao incentivar o desenvolvimento de negócios locais e distribuídos, o Serviço Digital Brasileiro impulsionará a economia em diferentes regiões do país, gerando empregos e estimulando o empreendedorismo na área de tecnologia.

e) Colaboração e inovação: A abordagem colaborativa e distribuída das plataformas digitais possibilitará a participação ativa de usuários, desenvolvedores e instituições, fomentando a colaboração, a troca de conhecimento e a inovação no âmbito tecnológico.

4. Próximos passos:

Sugerimos a criação de um grupo de trabalho para elaborar um plano estratégico detalhado e um cronograma de implementação para o Serviço Digital Brasileiro. Esse grupo de trabalho deve envolver representantes de instituições governamentais, acadêmicas, empresariais e da sociedade civil, a fim de garantir uma ampla participação e um alinhamento com os interesses e as demandas da sociedade brasileira.

Recomendamos, também, a realização de estudos de viabilidade técnica, econômica e legal, bem como uma análise mais aprofundada das tecnologias e soluções a serem adotadas, considerando aspectos como interoperabilidade, segurança, privacidade e escalabilidade.

Estamos à disposição para fornecer suporte adicional na elaboração e implementação dessa proposta, bem como para participar de discussões e contribuir com conhecimentos e experiências relevantes para o Serviço Digital Brasileiro.

Agradecemos pela atenção e ficamos à disposição para quaisquer esclarecimentos adicionais.

Atenciosamente,

Instituto Brasileiro de Políticas Digitais - Mutirão

Migrado de https://pad.riseup.net/p/ServicoDigitalBrasileiro-keep

