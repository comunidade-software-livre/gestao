## A proposta submetida foi a mais votada do Ministério de Ciência e Tecnologia

Na página 31 do [Relatório do 2o Fórum Interconselhos](https://gitlab.com/comunidade-software-livre/gestao/-/blob/main/Campanhas/PPA%20Participativo%202023/Resultados/8._Sistematiza%C3%A7%C3%A3o_das_Contribui%C3%A7%C3%B5es_do_II_F%C3%B3rum_Interconselhos_para_o_PPA_Participativo_2024-2027_19_de_julho.pdf) consta que a nossa proposta foi contemplada pelos programas Inovação nas Empresas para uma Nova Industrialização e  Ciência, Tecnologia e Inovação para o Desenvolvimento Social.

No [PPA](https://gitlab.com/comunidade-software-livre/gestao/-/blob/main/Campanhas/PPA%20Participativo%202023/Resultados/Projeto_de_Lei_completo.pdf)

O **PROGRAMA: 2324 - Inovação nas Empresas para uma Nova Industrialização** resultou nos objetivos:

- 0209 - Incentivar a colaboração entre ICTs e empresas para o desenvolvimento tecnológico e o aumento do conteúdo de inovação nacional

- 0210 - Ampliar os investimentos em P&D para estruturação e expansão dos complexos industriais-tecnológicos

- 0211 - Ampliar o custo-efetividade dos incentivos fiscais à PD&I (geridos pelo MCTI) nas empresas nacionais

- 0212 - Incentivar o desenvolvimento de ambientes inovadores e o empreendedorismo inovador

- 0524 - Fomentar a transformação digital, a capacitação digital, a estruturação e a expansão da utilização de TICs nos complexos industriais estratégicos para o desenvolvimento nacional


E o **PROGRAMA: 2304 - Ciência, Tecnologia e Inovação para o Desenvolvimento Social**, resultou nos objetivos:

- 0213 - Ampliar o fomento a projetos de Pesquisa & Desenvolvimento (P&D) construídos a partir do diálogo e em colaboração com atores não acadêmicos, e cujos conhecimentos coproduzidos sejam voltados à solução de problemas socialmente relevantes

- 0214 - Ampliar o desenvolvimento, o acesso, a reaplicação e a apropriação de tecnologias sociais

- 0215 - Promover as iniciativas de Popularização da Ciência & Tecnologia e Educação Científica no país

- 0216 - Promover o desenvolvimento, a produção nacional e a certificação de tecnologias assistivas

- 0217 - Fomentar a pesquisa, a extensão e o desenvolvimento científico e tecnológico na área de Soberania e Segurança
Alimentar e Nutricional (SSAN), gerando soluções inclusivas inovadoras para erradicação da fome e mitigação de desigualdades.
