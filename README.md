Olá! Somos um grupo de pessoas entusiastas da filosofia do Software livre.

Este é um espaço aberto para proposições, debates, dúvidas e construção coletiva de pautas que girem em torno de Software Livre.

Caso tenha interesse em contribuir conosco, por favor, leia nosso [código de conduta](https://gitlab.com/comunidade-software-livre/gestao/-/blob/main/codigo-de-conduta.md) e, caso concorde com as regras, faça a solicitação de entrar no grupo.

[Aqui](https://gitlab.com/comunidade-software-livre/gestao/-/blob/main/nossa-historia.md?ref_type=heads) tem um pouco da nossa história.

Usamos as _[issues](https://gitlab.com/comunidade-software-livre/gestao/-/issues)_ do _Gitlab_ para fazermos as nossas discussões. O acesso às discussões é aberto, mas as contribuições são restritas a participantes do grupo. Além disso, mantemos dois canais de comunicação principais:

- Sala na matrix: https://matrix.to/#/#comunidadesoftwarelivre:matrix.org

- Grupo no Telegram: https://t.me/ComunidadeSoftwareLivre
