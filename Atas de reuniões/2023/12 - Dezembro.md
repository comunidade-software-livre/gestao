Reunião de fim de ano

- Definição de critério para novo membro da comissão de gestão e administradores da comunidade
    - Caso alguém deseje entrar, será realizado um processo decisório interno dos membros atuais utilizando o método da sociocracia

- Regra de participação nas comissões para se manter como membro
    - Pessoas que não atuam serão consultadas se desejam continuar, mas caso continuem sem contribuir com a comissão serão excluídas
