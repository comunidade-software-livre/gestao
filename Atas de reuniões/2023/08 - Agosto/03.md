Presentes: Fred Guimarães, Fabs Balvedi, Lívia Gouvêa, Isabel Dias, Tiago Bugarin, Diego Rojas

- Quais os objetivos do grupo:

    - Espaço para colaborar no fomento para o uso do SL tanto do ponto de vista pessoal quanto do ponto de vista de governo. Pautar e agregar pessoas para pautar junto conosco em ambos os aspectos.
    - Grupo colaborativo autogestionário horizontal organizado em comissões, formada por relação de confiaça. 
    - Um local de convocação onde é possível montar "enxames" em específicos em momentos. Também um local de produção de vídeos, tutoriais, etc, para qualificar os nossos instrumentos de trabalho.
    - Conseguir comunicar para além da nossa bolha
    - Ter uma força de coalizão de conseguir fluir e capilazar para conseguir atingir lugares ainda não atingidos e conseguir se juntar em momentos em que precisamos enfrentar 

Ação: Iniciar o manifesto

Registro em áudio e vídeo desta reunião disponível em https://archive.org/details/Reuniao-Comunidade-Software-Livre-Brasil-2023-08-03
